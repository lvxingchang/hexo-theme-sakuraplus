# hexo-theme-sakuraplus

#### 介绍

主题是：hexo-theme-sakura，经过我的魔改，哈哈，plus版

Hexo(sakura)+gitee/github搭建个人博客（总结） https://blog.csdn.net/cungudafa/article/details/104457124

查看效果：https://cungudafa.top

#### 增加部分
首页：
- 首页大屏视频
- 随机封面图
- Lazyload图片懒加载
- 前端主题切换

Valine评论：
- QQ/Gravatar 头像
- 评论插图
- 用户 UA 及 IP 定位
- 每日诗词

文章：
- 文章目录压缩（随机编号或拼音）
- 不蒜子数字和访问量统计
- Mac 风格代码块
- 支持文章置顶和文章打赏
- 支持 MathJax
- TOC 目录
- 时间轴式的归档页
- 可设置复制文章内容时追加版权信息
- 可设置阅读文章时做密码验证
- 自定义词云
- 五星评分

其他：
- 定制个人logo
- live2d看板娘
- 天气插件
- botui机器人对话
- DaoVoice在线通讯
- 相册
- Aplayer/HermitX 支持（音乐播放）
- CDN 优化
- PicGo+github图床
- neat压缩

#### 参与贡献

cungudafa


